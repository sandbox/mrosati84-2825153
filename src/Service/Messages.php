<?php

namespace Drupal\messages_framework\Service;

/**
 * Messages service class.
 */
class Messages {

  /**
   * The message type.
   *
   * @var string
   */
  private $type;

  /**
   * Messages constructor.
   *
   * @param string $type
   *   The message type.
   */
  public function __construct($type = 'status') {
    $this->type = $type;
  }

  /**
   * Returns all messages that have been set.
   *
   * @param null $type
   *   (optional) Limit the messages returned by type.
   * @param bool $clear_queue
   *   (optional) If TRUE, queue will be cleared of messages of $type parameter.
   *
   * @return array
   *   An associative, nested array of messages grouped by message type.
   */
  public function get($type = NULL, $clear_queue = TRUE) {
    return drupal_get_messages($type, $clear_queue);
  }

  /**
   * Sets a message.
   *
   * @param string $message
   *   The message to set.
   * @param string $type
   *   Optional, the message type.
   */
  public function set($message, $type = NULL) {
    drupal_set_message($message, $type !== NULL ? $type : $this->type);
  }

}
